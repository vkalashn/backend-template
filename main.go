package main

import (
	"github.com/gin-gonic/gin"
	pluginSdk "gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/plugins/core"
	"net/http"
)

func handler(c *gin.Context) {
	c.JSON(http.StatusOK, `{"userId":1,"id":1,"title":"delectus aut autem","completed":false}`)
}

func main() {
	r := gin.Default()
	r.GET("/app", pluginSdk.AuthMiddleware(), handler)
	r.Run()
}
